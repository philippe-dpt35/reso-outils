# Les outils de déploiement

Plusieurs scripts ont été développés afin de faciliter le déploiement rapide de systèmes. Ces scripts permettent de réaliser des clones de systèmes ou de partitin, et de les restaurer.

Ce sont des surcouches de Clonezilla en mode graphique. On peutcloner ou restaurer un système en quelques clics, sans être contraint de passer par les divers menus complexes de Clonezilla.

## Déploiement RÉSO

![](assets/20220614_153244_deploiement.png)

Ce script sauvegarde ou restaure automatiquement un disque entier depuis ou vers le disque principal /dev/sda en recherchant la partition IMAGES-SYSTEMES qui sera montée dans le dossier /home/partimag qu'utilise Clonzilla.

Seule l'option "Restaurer une image sur un disque USB" permet de restaurer sur un autre disque monté en USB.

S'il n'existe pas de partition IMAGES-SYSTEMES, ou si l'on souhaite travailler sur un autre disque que /dev/sda, alors il faut utiliser Disk backup.

## Disk backup

![](assets/20220614_153357_disk-backup.png)

Ce script permet de sauvegarder ou de restaurer vers ou depuis n'importe quel disque (sauf bien entendu le disque conteant le système actif). Un sélecteur de fichiers permet de choisir n'importe quel répertoire pour la sauvegarde ou la restauration.

Ce script offre davantage de possibilités de sélection, mais nécessite de paser par davantage de boîtes de dialogue avant de réaliser l'opération. Le script Déploiement RÉSO permet donc d'aller beaucoup plus vite si l'on dispose d'une partition IMAGES-SYSTEMES contenant les images à déployer.

## Parts backup

![](assets/20220614_153428_parts-backup.png)

Ce script permet de ne travailler que sur des partitions et non des disques entiers.

## Infos systèmes

![](assets/20220614_160255_infos-systeme.png)

Ce script reprend les informations données par l'application Neofetch en lui ajoutant des informations sur la structure des périphériques de stockage.

## Intégration des scripts sur un système Debian ou dérivé

L'association utilise également une autre méthode que la clé USB pour effectuer ses déploiements. Elle utilise un PC sur lequel sont installés ces scripts et leurs dépendances. Ce PC  disposant de ports USB3 permet de réaliser les opérations plus rapidement sans avoir à booter sur la machine à réemployer, soit en démontant son disque dur pour le connecter avec un adaptateur SATA/USB, soit en connectant directement l'adaptateur au disque sans le démonter après avoir ouvert le boîtier.

Pour faciliter l'installation de tous les outils nécessaires sur un PC, l'association a créé le paquet outils-reso_xx.deb disponible dans ce répertoire.


