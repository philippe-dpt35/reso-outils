
# Personnalisation de l'image iso de Clonezilla

Pour cette partie, il sera nécessaire se savoir utiliser le logiciel Isomaster, ou tout autre logiciel permettant de modifier le contenu d'images iso.

Les scripts de déploiement RÉSO s'appuient sur les outils de sauvegarde et restauration de Clonezilla.

Nous allons personnaliser une iso de Clonezilla qui nous servira dans deux situations :

 - nous l'ajouterons aux images iso du répertoire ventoy de la clé USB
   afin de disposer d'un outil de déploiement en mode texte dans les cas
   où les systèmes que nous avons créé en mode graphique échoueraient au
   démarrage pour des incompatibilités matérielles; 
   
 - pour démarrer sur des machines vrtuelles avec les systèmes à déployer
   afin d'en réaliser une image que nous pourrons installer sur d'autres
   machines.

Les distributions live fonctionnent depuis un système en lecture seule compressé dans un fichier filesystem.squashfs.
  
Sur cette page du site de Clonezilla
https://clonezilla.org/downloads/download.php?branch=stable
sont proposées les différentes versions alternatives du système. Afin de disposer d'un système en mesure de démarrer sur de très vieilles machines, on choisira une version pour les machines ne supportant pas PAE. On sélectionne donc i686 dans le champ CPU architecture.

Il est proposé de télécharger le système au format zip ou iso. On télécharge les deux, puis on décompresse le zip dans un répertoire de travail de son choix.

Pour avoir Clonezilla directement en français et avec un clavier AZERTY sans avoir à passer par les menus de configuration, il faut modifier les fichiers /syslinux/syslinux.cfg pour le boot USB et /syslinux/isolinux.cfg pour le boot sur CD. Voici les modifications à effectuer.

Dans les options des entrées de menu (on peut se contenter de ne modifier que l'entrée correspondant au démarrage par défaut), après

    locales=

et

    keyboards_layout

on saisit

    locales=fr_FR.UTF-8
    keyboards_layout=fr

 ## Intégration des scripts clone-disque, restaure-disque, restaure-disque-usb, monte-images

Pour que les scripts soient disponibles comme commandes dans Clonezilla, il faut modifier le fichier filesystem.squashfs.
On le copie dans un second répertoire de travail auquel on donnera le nom que l'on veut, puis on le décompresse par

    sudo unsquashfs -d */dossier/décompression* filesystem.squashfs

 pour le décompresser dans le dossier /dossier/décompression qui ne doit pas exister préalablement ou

    sudo unsquashfs filesystem.squashfs

qui crée un répertoire squashfs-root contenant le système décompressé.

On se rend dans ce répertoire. On insère les scripts clone-disque, restaure-disque, restaure-disque-usb, monte-images dans le sous-répertoire /usr/bin.

Pour les anglophones irréductibles, on peut créer des liens symboliques afin de disposer de ces commandes en anglais. On se place dans le sous-répertoire /usr/bin et on y lance les commandes :

    ln -s clone-disque clone-disk
    ln -s restaure-disque restore-disk
    ln -s restaure-disque-usb restore-disk-usb
    ln -s monte-images mount-images

Il faudra faire la copie par sudo ou en root car le dossier de décompression de l'image appartient à root et n'est pas modifiable par les autres.

On reconstruit le filesystem compressé en passant la commande suivante dans le répertoire dans lequel se trouve le filesystem décompressé, squashfs-root si on a choisi la commande avec un nom de répertoire par défaut

    sudo mksquashfs . /chemin/fichier/filesystem.squashfs

avec /chemin/fichier correspondant au dossier dans lequel on veut obtenir le nouveau filesystem.squashfs.

Avec l'application isomaster qu'on aura préalablement installée, on ouvre le système Clonezilla téléchargé au format iso. On remplace le fichier filesystem.squashfs de cette iso par celui que l'on vient de modifier, et on supprime le fichier filesystem.squashfs.size.

On remplace également les fichiers /syslinux/syslinux.cfg et /syslinux/isolinux.cfg par ceux que l'on a précédemment modifiés.

On enregistre cette nouvelle iso avec un nom explicite de son choix, par exemple clonezilla-reso-nonPAE.iso

## Utilisation de Clonezilla personnalisé

 Au démarrage, un premier menu propose de choisir entre différents modes. On peut lancer celui par défaut. Un second menu propose de démarrer Clonezilla ou de passer en ligne de commande.
Pour utiliser les scripts personnalisés, il faut passer en ligne de commande.

**ATTENTION** : Clonezilla ne démarre pas en session root. Il faut donc lancer les commandes par sudo, ou passer en root par

    sudo su

si l'on ne veut pas avoir à lancer les commandes avec sudo

## Description des scripts
  
Pour cloner le disque sda, on lance la commande

`sudo clone-disque nom-image` ou   `sudo clone-disk nom-image`

*nom-image* correspondant au nom que l'on souhaite donner à l'image système.
  
Pour restaurer le disque sda :

`sudo restaure-disque nom-image` ou `sudo restore-disk nom-image`

*nom-image* devant correspondre à un nom d'image système existant

Pour afficher le contenu de la partition contenant les images système :

    ls -l

Si la partition n'a pas encore été montée, faire, avant de demander l'affichage du contenu :

`sudo monte-images` ou `sudo mount-images`

Pour restaurer un disque monté en USB : (pour afficher la liste des périphériques, lancer lsblk)

`sudo restaure-disque-usb nom-image /dev/sdx` ou `sudo restore-disk-usb nom-image /dev/sdx`

avec sdx devant correspondre au périphérique sur lequel restaurer

Pour créer un fichier swap au sein d'une partition système

`sudo cree-swapfile /dev/sdx taille` ou `sudo make-swapfile /dev/sdx taille`

avec */dev/sdx* la partition du système dans lequel créer le fichier swap,

*taille* une valeur entière en G ou M. Exemples : 2G    1500M