# Création de systèmes personnalisés pour le diagnostic, la réparation et le déploiement

![Aperçu du système](images/systeme.png)

Nous allons construire deux systèmes personnalisés de diagnostic, de réparation et de déploiement, l'un en 64 bits, l'autre en 32 bits, de façon à pouvoir démarrer sur tout type de PC, y compris très anciens.

Nous construirons ces systèmes sous forme d'iso en mesure de démarrer en live depuis une clé USB. Ces iso seront à placer dans le répertoire ventoy de la clé USB de démarrage telle que créée dans le dossier cle-usb.

Nous partirons d'une distribution MX Linux. Cette distribution, basée sur Debian, propose en effet un outil permettant de créer facilement une image iso du système qui nous permettra de la mettre facilement et rapidement à jour sur notre clé USB de déploiement.

Que ce soit en 64 ou en 32 bits, le principe de construction reste le même. Il suffit de partir de l'installation en machine virtuelle d'une MX Linux 64 ou 32 bits. Nous aurons donc à construire 2 machines virtuelles, l'une pour un système 64 bits, l'autre pour le système 32 bits.

## Création des machines virtuelles Virtualbox

Avec Virtualbox, on crée des machines virtuelles Linux Debian 64 et 32 bits. On choisira une taille de disque fixe de 20 Go. Il faut en effet prévoir une place suffisante pour le processus de création de l'iso.

Dans la partie stockage de la configuration, on insère une iso d'installation de MX Linux, puis on démarre la machine sur cette iso pour procéder à l'installation. On crée un utilisateur reso avec un mot de passe de son choix qu'on désactivera par la suite.

On part d'une MX Linux complète, et non d'une version lite.

## Personnalisation du système

Une fois le système installé, on procède à sa personnalisation.

On allège le système en supprimant des paquets dont nous n'aurons pas besoin pour notre usage.

sudo apt remove --purge libreoffice* conky-manager conky-toggle-mx mc mc-data mx-idevice-mounter seahorse onboard xfburn foliate pdfarranger geany gthumb gthumb-data simple-scan transmission-gtk transmission-common thunderbird gnome-ppp lbreakout2 lbreakout2-data gnome-mahjongg peg-e swell-foop asunder clementine gmtp webcamoid webcamoid-data webcamoid-plugins mx-welcome mx-tour job-scheduler timeshift mugshot pppoeconf system-config-printer gufw mx-docs blueman bluetooth bluez bluez-cups bluez-firmware bluez-obexd bluez-tools ddm-mx lazpaint-gtk2 lazpaint-qt5 mx-conky mx-codecs
On y installe divers outils utiles ou nécessaires pour le diagnostic, la réparation et le déploiement :

sudo apt install mpv clonezilla partclone yad neofetch testdisk gnome-disk-utility smartmontools gsmartcontrol hardinfo lshw-gtk xarchiver partimage
On nettoie :

sudo apt autoremove
sudo apt autoclean
sudo apt clean
On installe tous les scripts RESO ainsi que les icônes et le fond d'écran : deploiement-reso, infosys, infosys.desktop, disk-backup, parts-backup, deploiement-reso.desktop, disk-backup.desktop, parts-backup.desktop, RESO-1600x1200.svg
Pour cela on récupère l'archive scripts-reso-mx.tar que l'on décompresse. On ouvre un terminal dans le dossier où l'archive a été décompressée puis on lance les commandes suivantes :

sudo cp deploiement-reso disk-backup parts-backup /usr/bin/
sudo cp deploiement-reso.desktop disk-backup.desktop parts-backup.desktop /usr/share/applications
sudo cp RESO-1600x1200.svg /usr/share/backgrounds
On désactive le mot de passe de l'utilisateur reso par

sudo passwd -d reso
## Personnalisation de l'interface

On pourra personnaliser la barre des tâches selon ses habitudes de travail : positionnement, ajout ou retrait de lanceurs.

**A noter** : une fois cette personnalisation effectuée sur l'une des VM, on pourra la reconstituer automatiquement sur l'autre VM en récupérant le dossier ~/.config/xfce4 pour remplacer celui de l'autre VM.

## Changement du fond d'écran de la session

Clic droit sur le bureau, paramètres du bureau. On sélectionne l'image /usr/share/backgrounds/RESO-1600x1200.svg

**ATTENTION**, cette solution ne fonctionne pas toujours. Sur certains PC, c'est l'image originale qui réapparait. Il faut modifier le fichier /home/reso/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml ainsi que celui du gestionnaire de connexion /etc/lightdm/lightdm-gtk-greeter.conf

## Création de lanceurs sur le bureau

Dans le menu, on fait un clic droit sur chacune des entrées de menu suivantes en demandant à l'ajouter au bureau :

* Sous-menu système
  Déploiement RÉSO
  Disk backup
  Parts backup
  Infos système
  Hardware lister
* Sous-menu MX outils
  Informations système

## Création du live

Une fois le système correctement paramétré et configuré à sa covenance, on en crée un live iso.

Pour démarrer automatiquement l'iso avec un clavier azerty et en langue française, il faudra intervenir sur le fichier /etc/mx-snapshot.conf.

Modifier la ligne

    edit_boot_menu=no
en

    edit_boot_menu=yes

Pour construire l'iso nous utilisons l'outil mx-snaphots depuis le menu principal, sous-menu Outils MX. Grâce aux modifications effectuées dans le fichier /etc/mx-snapshot.conf, il sera proposé d'éditer le menu de démarrage en cours de construction. Il faudra répondre positiveement lorsque la question sera posée, et un éditeur de texte s'ouvrira avec le fichier isolinux.cfg. Depuis cet éditeur de texte, ouvrir également les fichiers  syslinux/syslinux.cfg et grub/grubenv.cfg auquels on pourra accéder en remontant d'un répertoire dans la boîte de sélection de fichiers qui s'affiche..

Dans les fichiers isolinux.cfg et syslinux.cfg, dans la section live, à la ligne APPEND, remplacer `quiet `par

    lang=fr

Dans grubenv.cfg, ajouter

    kbd=fr
    tz=Europe/Paris
    lang=fr_FR

**Nota** : il existe une autre solution permettant de conserver ces paramètres sans être contraint de le refaire à chaque création d'iso. Mx-snaphot crée ces 3 fichiers en utilisant l'archive /lib/iso-template/iso-template.tar.gz. On peut donc modifier ces 3 fichiers dans l'archive. Mais attention : cette archive fait partie du paquet mx-iso-template, qui est une dépendance du paquet mx-snapshot. Une mise à jour écrasera donc cette archive avec les nouveaux paramètres. Dès lors il conviendra de faire une copie de l'archive modifiée pour la rétablir après mise à jour.

L'image peut être récupérée dans le dossier /home/snapshot. Elle est à placer dans le répertoire ventoy de la clé USB. Ventoy se chargera alors de créer automatiquement un menu à partir des iso présentes dans ce dossier.
