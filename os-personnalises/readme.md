# Création de systèmes personnalisés pour le déploiement

Pour cette partie, il est nécessaire de savoir utiliser le logiciel Virutalbox ou tout autre logiciel de gestion de machines virtuelles.

## Les systèmes utilisés par RÉSO

L'association RÉSO propose d'installer différents systèmes en fonction des besoins des bénéficiaires.
Voici les systèmes principalement utilisés :

### Linux mint mate

Pour les personnes seules ou les familles sans jeunes enfants ayant besoin d'un OS généraliste pour les usages numériques courants.
Cette distribution est déclinée en deux versions selon l'âge des machines fournies : une UEFI pour les PC récents, une Legacy pour les PC plus anciens.

### PrimTux

Pour les familles ayant des enfants en scolarité primaire, ainsi que pour les écoles.
Plusieurs versions sont proposées selon les besoins.
PrimTux7 ubuntu avec contrôle parental préinstallé, en version UEFI ou en version Legacy, pour les familles.
PrimTux7 ubuntu sans contrôle parental préinstallé pour les écoles.
PrimTux6 debian9 avec chromium à la place de Firefox pour les très vieilles machines pouvant être propossées comme machines secondaires éducatives pour les enfants en milieu familial, sans être connectées à Internet.

### Q4OS

Pour les particuliers ou familles ayant besoin d'un système généraliste à la fois moderne et léger sur un PC portable très ancien.

## Construction des systèmes

On construit les différents systèmes dans des machines virtuelles avec le logiciel Virtualbox.
Se référer aux tutoriels de Virtualbox pour son utilisation.

Quelques conseils, et règles à respecter :

* La machine de développement doit être assez puissante et disposer d'une capacité conséquente de RAM afin de pouvoir travailler sur plusieurs VM à la fois. 16 Go paraît être une base minimale.
* Pour la configuration, mettre en place un dique de taille fixe et non dynamique sinon la réalisation de l'image système échouera.
* Pour le réseau, choisir de préférence un accès par pont.
* Pour la construction de machines en UEFI, bien cocher EFI dans la configuration du système.
* Pour faciliter le déploiement quii se fera forcément sur des disques durs de taille supérieure, il faudra faire des installations avec une swap dans un fichier (par défaut sur les systèmes ubuntu et dérivés) et non dans une partition séparée. En effet lors dela restauration les partitions seront agrandies poportionnellement. Dès lors si l'on a construit un VM sur un disque de 10 Go avec une partition swap de 2 Go, on se retrouvera avec une partition swap de 100 Go si l'on déploie sur un disque de 500 Go !

Après avoir installé le système, il est possible de le personnaliser en fonction des besoins de l'association pour l'adapter aux publics rencontrés.
Il suffit ensuite de le mettre à jour régulièrement et d'en refaire une image système pour le déploiement. Cela permet de distribuer des équipements avec des systèmes à jour.

## Réalisation d'une image système

Dans la partie configuration --> stockage des paramètres de la VM, insérer l'image iso de clonezilla personnalisé avec les scripts RÉSO (tel qu'indiqué dans le dossier clonezilla-reso) dans le dispositif IDE.
On insère la clé USB de déploiement sur le PC, et on s'assure que cette clé soit activée dans la configuration USB des paramètres de la VM. Attention : il faut bien cocher le contrôleur correspondant au type de port USB utilisé, par défaut c'est un port USB2 qui est coché !

On démarre la VM sur l'iso de clonezilla RÉSO, et on choisit de passer en ligne de commande.
Si l'on souhaite voir la liste des images systèmes déjà présentes sur la clé, on peut lancer
`monte-images` ou `mount-images`

Pour réaliser l'image système, il suffit de lancer
`clone-disque nom-image` ou `clone-disk nom-image`

avec *nom-image* le nom que l'on souhaite donner à cette image.

L'image se créera automatiquement sur la partition IMAGES-SYSTEMES de la clé USB.
