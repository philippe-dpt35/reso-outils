#!/bin/bash

####################################
# usage, en root :
# swapfile.sh taille
# Exemples :
# swapfile.sh 1500m
# swapfile.sh 2G
###################################

set -e

if [ -z "$1" ]; then
  echo -e "Indication de taille de fichier manquante.\nUsage, en root :\nswapfile.sh taille\nExemples :\nswapfile.sh 1500m\nswapfile.sh 2G"
  exit 1
fi

fallocate -l "$1" /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile

if result=$(! grep '^/swapfile' /etc/fstab); then
  echo "/swapfile   none   swap   sw   0   0" >> /etc/fstab
fi

if [ ! -e /etc/sysctl.d/swappiness10.conf ]
  then echo "vm.swappiness = 10" > /etc/sysctl.d/swappiness10.conf
fi

exit 0