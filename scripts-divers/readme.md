lémentaires divers

**swapfile.sh**
Crée un fichier de swap de la taille indiquée dans le système actif, et règle le swappiness à 10.
usage, en root : swapfile.sh *taille*
Exemples :    `sh swapfile.sh 1500m`      `sh swapfile.sh 2G`

**swappiness10.sh**
Fixe le déclenchement du swap à 90% d'utilisation de la RAM (swappiness = 10)
