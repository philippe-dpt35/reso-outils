#!/bin/bash

# Fixe le swappiness à 10

if [ ! -e /etc/sysctl.d/swappiness10.conf ]
  then echo "vm.swappiness = 10" > /etc/sysctl.d/swappiness10.conf
  systemctl restart systemd-sysctl.service
fi