# RÉSO

Ce git explique comment construire les divers outils utilisés par l'association RÉSO pour le déploiement rapide de systèmes d'exploitation.

Les explications nécessaires à la construction de chacun des outils sont données dans les divers dossiers.

## cle-usb

Comment créer une clé USB permettant de démarrer sur des OS personnalisés contenant des outils de diagnostic, de réparation et de déploiement.

## os-reso

Comment construire chacun des OS personnalisés utilisés par l'association, qui seront placés sur la clé USB de déploiement.

## clonezilla-reso

Comment construire des systèmes personnalisés de clonezilla facilitant le clonage et la restauration de systèmes.

## outils-deploiement

Comment installer les outils de déploiement RÉSO sur un PC avec un système Debian ou dérivé installé, de façon à pouvoir faire des déploiements depuis ce poste.

## scripts-divers

Divers scripts complémentaires pouvant être utiles lors des déploiements ou pour des personnalisations de systèmes.

## os-personnalises

Comment construire des OS personnalisés qui pourront être facilement déployés sur des PC.

## En pratique :

1. On construit la clé USB de déploiement comme expliqué dans le dossier cle-usb.

2. On construit les systèmes live personnalisés de diagnostic, réparation et déploiement tel qu'expliqué dans le dossier os-reso. On place les iso obtenues dans le dossier ventoy de la clé USB.
   
3. On construit un système live clonezilla personnalisé avec les scripts développés par l'association, tel qu'expliqué dans le dossier clonezila-reso. On place l'iso obtenue dans le dossier ventoy de la clé USB. On conserve une autre iso accessible pour démarrer dessus depuis des machines virtuelles.
   
4. On construit les systèmes que l'on souhaite déployer dans des machines virtuelles tel qu'expliqué dans le dossier os-personnalises. On en réalise des clones sous forme d'images que l'on place sur la partition IMAGES-SYSTEMES de la clé USB.
   
5. Si l'on souhaite effectuer un déploiement sur des disques montés en USB depuis un PC, on suit les explications données dans le dossier outils-deploiement.
