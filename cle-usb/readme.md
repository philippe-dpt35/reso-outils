# Création de la clé USB

Cette clé USB permettra de démarrer un OS personnalisé contenant des outils de diagnostic, de réparation, et de déploiement de systèmes.

Cette clé, s'appuyant sur l'outil Ventoy, sera en mesure de démarrer aussi bien sur des machines en UEFI qu'en Legacy.

On télécharge l'outil Ventoy depuis cette page :

https://www.ventoy.net/en/download.html

On peut choisir l'archive au format zip ou tar.gz indifféremment.

On décompresse cette archive qui crée un dossier Ventoy-x.x.x

## Méthode automatique

On copie le script cle-reso dans le dossier de Ventoy que l'on vient de décompresser. On donne les droits en exécution à ce fichier, puis on ouvre un terminal dans le dossier et on lance

`sudo ./cle-reso`

Cela crée 3 partitions sur la clé, une partition EFI à laquelle on ne touche pas, une partition Ventoy sur laquelle on copiera les .iso des systèmes live sur lesquels on souhaite démarrer (voir le dossier os-reso), et une partition IMAGES-SYSTEMES sur laquelle on copiera toutes les images systèmes que l'on souhaite déployer (voir le dossier os-personnalises).

Par défaut, ce script réserve une taille de 10 Go pour la partition ventoy contenant les iso de boot. On peut de manière optionnelle modifier la taille de cette partition. Pour cela on lance le script en indiquant la taille souhaitée par une valeur numérique en Mo.
Exemple :

`sudo ./cle-reso 5000`

créera la clé en attribuant une taille de 5Go à la partition Ventoy.

## Méthode manuelle

Dans le dossier de Ventoy se trouve un script permettant de créer automatiquement en ligne de commande une clé de démarrage Ventoy, `Ventoy2Disk.sh`.

La syntaxe de la commande est la suivante :

Ventoy2Disk.sh -i /dev/sdx

L'option -i demande une installation (il est proposé une option -u pour mettre à jour une installation existante)

/dev/sdx désigne le chemin du périphérique USB sur lequel doit se faire l'installation, probablement /dev/sdb, mais c'est à vérifier.

En l'état cette commande crée 2 partitions :

- VENTOYEFI, partition EFI qui contient les outils Ventoy permettant le boot ;
- Ventoy, partition sur laquelle seront à placer les iso des systèmes ;

sur lesquelles on souhaite démarrer. Ventoy créera automatiquement un menu de boot à partir des fichiers .iso présents.

Pour RÉSO, nous aurons besoin d'une partition supplémentaire pour y placer les images systèmes à déployer. Le script permet de faire cela facilement, en réservant un espace défini grâce à l'option -r

Prenons l'exemple d'une clé de 128 GO sur laquelle nous voulons réserver un espace de 100 Go pour nos images systèmes. Nous allons lancer la commande suivante en root :

sudo sh Ventoy2Disk.sh -i -r 100000 /dev/sdb

Avec Gparted, nous créons ensuite une partition ext4 ou NTFS de la taille de l'espace libre, en lui attribuant l'étiquette IMAGES-SYSTEMES.

Attribuer cette étiquette est indispensable, les scripts de déploiement l'utilisant pour localiser les images systèmes à déployer.

Si l'on a créé une partition ext4, il faudra lui donner les droits en lecture et écriture pour tous :

sudo chmod 777 /chemin/repertoire/IMAGES-SYSTEMES
où `/chemin/repertoire/` sera à modifier pour correspondre au chemin d'accès à cette partition.
